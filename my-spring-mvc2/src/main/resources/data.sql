INSERT INTO product (PRODUCT_NAME) VALUES ('Camera A');
INSERT INTO product (PRODUCT_NAME) VALUES ('Camera B');

INSERT INTO feature (FEATURE_NAME, DATA_TYPE) VALUES ('Description', 'STRING');
INSERT INTO feature (FEATURE_NAME, DATA_TYPE) VALUES ('Megapixels', 'NUMERICAL');

INSERT INTO product_details (PRODUCT_ID, FEATURE_ID, VALUE) VALUES (1, 1, 'Camera Description 1');
INSERT INTO product_details (PRODUCT_ID, FEATURE_ID, VALUE) VALUES (1, 2, '20');

INSERT INTO product_details (PRODUCT_ID, FEATURE_ID, VALUE) VALUES (2, 1, 'Camera Description 2');
INSERT INTO product_details (PRODUCT_ID, FEATURE_ID, VALUE) VALUES (2, 2, '18');