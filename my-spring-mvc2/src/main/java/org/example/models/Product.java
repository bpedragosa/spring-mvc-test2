package org.example.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "product")
public class Product implements Serializable{

    private int id;
    private String name;
    private List<ProductDetails> productDetails;

    public Product(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Product(){
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PRODUCT_ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    @Column(name = "PRODUCT_NAME", nullable = false)
    public String getName() {
        return name;
    }

    @OneToMany(cascade=CascadeType.ALL, mappedBy = "product", fetch = FetchType.LAZY)
    public List<ProductDetails> getProductDetails() {
        return productDetails;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setProductDetails(List<ProductDetails> productDetails) {
        this.productDetails = productDetails;
    }
}
