package org.example.models;

import javax.persistence.*;

@Entity
@Table(name = "feature", uniqueConstraints = {
        @UniqueConstraint(columnNames = "FEATURE_NAME")
})
public class Feature {

    private int id;
    private String name;
    private FeatureDataType dataType;
    
    public Feature(String name, FeatureDataType dataType) {
        this.name = name;
        this.dataType = dataType;
    }

    public Feature(){
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "FEATURE_ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    @Column(name = "FEATURE_NAME", unique = true, nullable = false)
    public String getName() {
        return name;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "DATA_TYPE", nullable = false)
    public FeatureDataType getDataType() {
        return dataType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDataType(FeatureDataType dataType) {
        this.dataType = dataType;
    }
}
