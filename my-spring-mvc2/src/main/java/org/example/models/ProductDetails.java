package org.example.models;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by bpedragosa on 3/24/16.
 */

@Entity
@Table(name = "product_details", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"PRODUCT_ID","FEATURE_ID"})
})
public class ProductDetails implements Serializable {

    private int id;
    private Product product;
    private Feature feature;
    private String value;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ID")
    public Product getProduct() {
        return product;
    }

    @OneToOne
    @JoinColumn(name = "FEATURE_ID")
    public Feature getFeature() {
        return feature;
    }

    @Column(name = "VALUE", nullable = true)
    public String getValue() {
        return value;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", unique = true, nullable = false)
    public int getId() {
        return id;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public void setFeature(Feature feature) {
        this.feature = feature;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void setId(int id) {
        this.id = id;
    }
}
