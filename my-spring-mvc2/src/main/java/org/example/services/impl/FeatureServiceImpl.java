package org.example.services.impl;

import org.example.models.Feature;
import org.example.repositories.FeatureRepository;
import org.example.services.FeatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by bpedragosa on 3/24/16.
 */

@Service
public class FeatureServiceImpl implements FeatureService{

    @Autowired
    private FeatureRepository featureRepository;

    @Override
    public Feature saveFeature(Feature feature) {

        return featureRepository.save(feature);
    }
}
