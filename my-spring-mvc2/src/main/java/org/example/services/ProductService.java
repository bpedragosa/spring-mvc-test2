package org.example.services;

import org.example.models.Product;

import java.util.List;

/**
 * Created by bpedragosa on 3/24/16.
 */
public interface ProductService {
    Product getProductById(Integer id);
    Product saveProduct(Product product);
    List<Product> findAllProducts();
}
