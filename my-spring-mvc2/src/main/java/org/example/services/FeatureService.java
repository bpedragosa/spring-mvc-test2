package org.example.services;

import org.example.models.Feature;

/**
 * Created by bpedragosa on 3/24/16.
 */
public interface FeatureService {
    Feature saveFeature(Feature feature);
}
