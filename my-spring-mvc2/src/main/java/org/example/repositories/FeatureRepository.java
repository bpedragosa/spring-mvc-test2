package org.example.repositories;

import org.example.models.Feature;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by bpedragosa on 3/24/16.
 */
public interface FeatureRepository extends CrudRepository<Feature, Integer>{
}
