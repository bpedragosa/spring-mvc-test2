package org.example.repositories;

import org.example.models.Product;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by bpedragosa on 3/24/16.
 */
public interface ProductRepository extends CrudRepository<Product, Integer> {
}
