package org.example.controllers;

import org.example.models.Feature;
import org.example.models.FeatureDataType;
import org.example.models.Product;
import org.example.models.ProductDetails;
import org.example.services.FeatureService;
import org.example.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ProductService productService;

    @RequestMapping("/")
    public String greeting(Model model) {
        model.addAttribute("name", "World!");
        return "index";
    }

    @RequestMapping("/product/{productId}")
    public String viewProductDetails(Model model, @PathVariable("productId") String productId){
        Product product = null;

        if(productId != null){
            product = productService.getProductById(Integer.parseInt(productId));
        }
        model.addAttribute("product", product);
        model.addAttribute("name", "World!");

        return "index";
    }
}
